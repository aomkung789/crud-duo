/* eslint-disable no-unused-vars */
const User = require('../model/User')
const userController = {
  userList: [
    {
      id: 1,
      name: 'Pakawat K',
      gender: 'M',
      birthday: '1989-02-01',
      position: 'Dev',
      email: 'test1@hotmail.com'
    },
    {
      id: 2,
      name: 'Threerawen W',
      gender: 'M',
      birthday: '1990-11-04',
      position: 'SA',
      email: 'test2@hotmail.com'
    }
  ],
  lastId: 3,
  async addUser(req, res, next) {
    const payload = req.body
    const user = new User(payload)
    try {
      user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser(req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser(req, res, next) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async getUsers(req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)

    } catch (err) {
      res.status(500).send(err)
    }
  },

  async getUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
