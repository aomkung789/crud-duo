const express = require('express');
const app = express.Router();
const userController = require('../controller/UsersController')

app.get('/', userController.getUsers);

app.get('/:id', userController.getUser);

app.post('/', userController.addUser);

app.put('/', userController.updateUser);

app.delete('/:id',userController.deleteUser);

module.exports = app
