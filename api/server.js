// eslint-disable-next-line no-unused-vars
const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello!!' })
})
// eslint-disable-next-line no-undef
app.listen(8000, () => {
  console.log('Application is running on http://127.0.0.1:8000/')
})
